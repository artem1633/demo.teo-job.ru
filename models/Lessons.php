<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lessons".
 *
 * @property int $id
 * @property int $group_id Група
 * @property int $step № шага
 * @property string $hint Текст подсказки
 */
class Lessons extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lessons';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group_id', 'step'], 'integer'],
            [['hint'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Група',
            'step' => '№ шага',
            'hint' => 'Текст подсказки',
        ];
    }

    public static function getLessonsGroup ($group)
    {
        return static::find()->where(['group_id'=>$group])->asArray()->all();
    }

    public static function getGroupList()
    {
        return [
            1=>'Показатели',
            2=>'Тесты 1',
            3=>'Тесты 2',
            4=>'Тесты 3',
            5=>'Внутри теста 1',
            6=>'Внутри теста 2',
            7=>'Внутри теста 3',
            8=>'Результаты',
        ];
    }
}
