<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\base\AppActiveQuery;
use yii\web\ForbiddenHttpException;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "questionary".
 *
 * @property int $id
 * @property int $active Активность
 * @property string $name Название
 * @property string $description Описание
 * @property int $vacancy_id Вакансия
 * @property string $link Короткая ссылка
 * @property int $user_id Компания
 * @property string $date_cr Дата создание
 * @property string $date_up Дата изменение
 * @property int $access Доступ
 * @property int $show_in_desktop Показывать информацию на рабочем столе
 * @property int $count
 * @property int $filling_count
 * @property int $is_template
 * @property int $publish_company Публиковать на страницы компании
 * @property int $type Тип
 * @property int $time_test Время для теста
 * @property int $publish_answer Публиковать результаты в магазин
 * @property double $result_resume_sum Стоимость одного результата
 *
 * @property Alert[] $alerts
 * @property Users $user
 * @property Vacancy $vacancy
 * @property Questions[] $questions
 * @property Resume[] $resumes
 * @property SettingResult[] $settingResults
 */
class Questionary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questionary';
    }

    public static function find()
    {
        if(Yii::$app->user->identity->type != 0){
            $user_id = Yii::$app->user->identity->id;
        } else {
            $user_id = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'user_id' => $user_id,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false) {
            if(Yii::$app->user->identity->type !== 0)
            {
                $user_id = Yii::$app->user->identity->id;
                if($model->user_id != $user_id){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public $questionaryCount;
    public $template;
    public $buttons;
    public $step;
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['link'], 'unique'],
            [['user_id', 'access', 'show_in_desktop', 'active', 'count', 'filling_count', 'is_template','publish_company','type', 'step', 'time_test', 'publish_answer'], 'integer'],
            [['date_cr', 'date_up'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['result_resume_sum'], 'number'],
            [['link'], 'string', 'max' => 8],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
            /*[['vacancy_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vacancy::className(), 'targetAttribute' => ['vacancy_id' => 'id']],*/
            ['vacancy_id', 'validatVacancy'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание теста',
            'vacancy_id' => 'Тип пользователя',
            'link' => 'Короткая ссылка',
            'user_id' => 'Компания',
            'date_cr' => 'Дата создание',
            'date_up' => 'Дата изменение',
            'access' => 'Доступ',
            'show_in_desktop' => 'Показывать информацию на раб. столе',
            'active' => 'Активность',
            'count' => 'Количество посещений',
            'filling_count' => 'Количество заполнений',
            'questionaryCount' => 'Кол-во резюме',
            'is_template' => 'Шаблон',
            'template' => 'Выберите',
            'buttons' => 'Кнопки',
            'publish_company' => 'Публиковать на страницы компании',
            'type'=>'Тип теста',
            'time_test'=>'Время теста (мин.)',
            'publish_answer' => 'Публиковать результаты в магазин',
            'result_resume_sum' => 'Стоимость одного результата',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->user_id = Yii::$app->user->identity->id;
            $this->date_cr = date('Y-m-d H:i:s');
            $questionary = Questionary::find()->where(['user_id' => Yii::$app->user->identity->id, 'active' => 1])->one();
            if($questionary == null){
                $this->active = 1;
            }
            else{
                $this->active = 0;
            }

            $length = 8;
            $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

            do {
                $string ='';
                for( $i = 0; $i < $length; $i++) {
                    $string .= $chars[rand(0,strlen($chars)-1)];
                }
            } while ( Questionary::find()->where(['link' => $string])->one() != null );

            $this->link = $string;

        }
        $this->date_up = date('Y-m-d H:i:s');

        return parent::beforeSave($insert);
    }


    public function afterSave($insert, $changedAttributes)
    {

        if ($insert) {            
            $question = new Questions();
            $question->questionary_id = $this->id;
            $question->name = 'Аватар';
            $question->require = 0;
            $question->description = 'Фотография повышен на 70% вероятность просмотра вашего резюме!';
            $question->type = 6;
            $question->save();
            
            $question = new Questions();
            $question->questionary_id = $this->id;
            $question->name = 'ФИО';
            $question->require = 1;
            $question->type = 0;
            $question->save();

        } else {
            //Yii::$app->session->setFlash('success', 'Запись обновлена');
        }


        parent::afterSave($insert, $changedAttributes);

    }

    public function beforeDelete()
    {
        $questions = Questions::find()->where(['questionary_id' => $this->id])->all();
        foreach ($questions as $value) {
            $value->delete();
        }

        $alert = Alert::find()->where(['questionary_id' => $this->id])->all();
        foreach ($alert as $value) {
            $value->delete();
        }

        $resume = Resume::find()->where(['questionary_id' => $this->id])->all();
        foreach ($resume as $value) {
            $value->delete();
        }
        Directory::deleteDirectory($this->id, 'questions');
        return parent::beforeDelete();
    }

    //Новая вакансия
    public function validatVacancy($attribute, $params)
    {
        $vacancy = Vacancy::find()->where(['id' => $this->vacancy_id])->one();
        if (!isset($vacancy))
        {
            $vacancy = new Vacancy();
            $vacancy->name = $this->vacancy_id;
            
            if ($vacancy->save())
            {
                $this->vacancy_id = $vacancy->id;
            }
            else
            {
                $this->addError($attribute,"Не создано новая вакансия");
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlerts()
    {
        return $this->hasMany(Alert::className(), ['questionary_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVacancy()
    {
        return $this->hasOne(Vacancy::className(), ['id' => 'vacancy_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Questions::className(), ['questionary_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettingResults()
    {
        return $this->hasMany(SettingResult::className(), ['questionary_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResumes()
    {
        return $this->hasMany(Resume::className(), ['questionary_id' => 'id']);
    }

    public function getAccessList()
    {
        return [
            1 => 'Да',
            0 => 'Нет',
        ];
    } 

    public function getShowList()
    {
        return [
            1 => 'Да',
            0 => 'Нет',
        ];
    }

    public function getType()
    {
        return [
            1 => 'Анкета',
            2 => 'Тест',
        ];
    }
   
    public function getPublishCompany()
    {
        if($this->publish_company == 1) return 'Да';
        else return 'Нет';
    }

    public function getTypeDesription()
    {
        if($this->type == 1) return 'Анкета';
        else return 'Тест';
    }

    public function getAccessDesription()
    {
        if($this->access == 1) return 'Да';
        else return 'Нет';
    }

    public function getShowDesription()
    {
        if($this->show_in_desktop == 1) return 'Да';
        else return 'Нет';
    }
    public function getPublishDesription()
    {
        if($this->publish_company == 1) return 'Да';
        else return 'Нет';
    }
    public function getVacancyList()
    {
        $vacancy = Vacancy::find()->all();
        return ArrayHelper::map( $vacancy , 'id', 'name');
    }

    public function getUsersList()
    {
        $users = Users::find()->all();
        return ArrayHelper::map( $users , 'id', 'fio');
    }

    public function getTemplates()
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT * FROM questionary WHERE is_template = 1");
        $templates = $command->queryAll();
        $result = [];
        foreach ($templates as $value) {
            $result [] = [
                'id' => $value['id'],
                'value' => $value['name']
            ];
        }

        return ArrayHelper::map($result, 'id', 'value');
    }

    public function getQuestionsValue()
    {
        $query = Questions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder'=>'ordering ASC'],
            'sort' => ['defaultOrder' => ['ordering'=>SORT_ASC]]
            //'sort' => false,
        ]);

        $query->andFilterWhere(['questionary_id' => $this->id]);
        return $dataProvider;
    }

    public function getNextArrow()
    {
        return '<img alt="forward" style="height: 25px;" data-tip="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDhweCIgaGVpZ2h0PSI0OHB4IiB2aWV3Qm94PSIwIDAgNDggNDgiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8cG9seWdvbiBmaWxsPSIjRkZGRkZGIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgyNC4wMDAwMDAsIDI0LjAwMDAwMCkgc2NhbGUoLTEsIDEpIHRyYW5zbGF0ZSgtMjQuMDAwMDAwLCAtMjQuMDAwMDAwKSAiIHBvaW50cz0iNDAgMjIgMTUuNjYgMjIgMjYuODQgMTAuODIgMjQgOCA4IDI0IDI0IDQwIDI2LjgyIDM3LjE4IDE1LjY2IDI2IDQwIDI2Ij48L3BvbHlnb24+Cjwvc3ZnPg==">';
    }

    public function getPreviousArrow()
    {
        return '<img alt="back" style="height: 25px;" data-tip="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDhweCIgaGVpZ2h0PSI0OHB4IiB2aWV3Qm94PSIwIDAgNDggNDgiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8cG9seWdvbiBmaWxsPSIjRkZGRkZGIiBwb2ludHM9IjQwIDIyIDE1LjY2IDIyIDI2Ljg0IDEwLjgyIDI0IDggOCAyNCAyNCA0MCAyNi44MiAzNy4xOCAxNS42NiAyNiA0MCAyNiI+PC9wb2x5Z29uPgo8L3N2Zz4=">';
    }

}
