<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SettingResult;

/**
 * SettingResultSearch represents the model behind the search form about `app\models\SettingResult`.
 */
class SettingResultSearch extends SettingResult
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'questionary_id', 'condition'], 'integer'],
            [['first_value', 'second_value'], 'number'],
            [['text'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $query = SettingResult::find()->where(['questionary_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'questionary_id' => $this->questionary_id,
            'condition' => $this->condition,
            'first_value' => $this->first_value,
            'second_value' => $this->second_value,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
