<?php 

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Resume;

/**
 * ResumeSearch represents the model behind the search form about `app\models\Resume`.
 */
class ResumeSearch extends Resume
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'questionary_id', 'group_id', 'status_id','category', 'mark', 'new_sms', 'vacancy_id', 'correspondence', 'balls', 'ball_for_question', 'show_in_shop', 'user_id', 'buyed', 'doptest'], 'integer'],
            [['fit', 'date_cr', 'is_new', 'code', 'telegram_chat_id', 'connect_telegram', 'values', 'fio', 'ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $questionaryId, $post)
    {
        $resumeId = [];
        $check = 0;
        $session = Yii::$app->session;
        if(isset($_GET['page']) && $post['name'] === null) $post['name'] = $session['name'];

        if($post != null && isset($post['name']) ){
            $session['name'] = $post['name'];

            $yesNo = Resume::validateTrue($post['name']);
            $allResume = Resume::find()
                ->joinWith('questionary', true)
                ->joinWith('group', true)
                ->joinWith('status', true)
                ->joinWith('vacancy',true)
                ->andWhere(
                    ['or',
                       ['LIKE', 'resume.fio', $post['name']],
                       ['LIKE', 'questionary.name', $post['name']],
                       ['LIKE', 'group.name', $post['name']],
                       ['LIKE', 'resume_status.name', $post['name']],
                       ['LIKE', 'vacancy.name', $post['name']],
                       ['LIKE', 'resume.mark', $post['name']],
                       ['LIKE', 'resume.ball_for_question', $post['name']],
                       ['LIKE', 'resume.code', $post['name']],
                       ['LIKE', 'resume.correspondence', $post['name']],
                       ['LIKE', 'resume.telegram_chat_id', $post['name']],
                       ['LIKE', 'resume.category', $post['name']],
                       ['LIKE', 'resume.date_cr', date('Y-m-d H:i:s', strtotime($post['name']) )],
                       ['LIKE', 'resume.fit', $yesNo],
                       ['LIKE', 'resume.new_sms', $yesNo],
                       ['LIKE', 'resume.connect_telegram', $yesNo],
                       ['LIKE', 'resume.is_new', $yesNo],
                    ])
                ->all();

            foreach ($allResume as  $value) {
                $resumeId [] = $value->id;
            } 


            $resumes = Resume::find()->all();
            foreach ($resumes as $model) {
                foreach (json_decode($model->tags) as $value) {
                    $tag = Tags::find()->where(['LIKE','name', $post['name'] ])->andWhere(['id' => $value->id ])->one();
                    if($tag != null) $resumeId [] = $model->id;
                }
            }

            foreach ($resumes as $model) {
                foreach (json_decode($model->values) as $value) {
                    if( strpos( $value->value, json_encode($post['name']) ) !== false) $resumeId [] = $model->id;
                    if( strpos( $value->value, $post['name'] ) !== false) $resumeId [] = $model->id;
                }
            }
        }
        else{
            $resumes = Resume::find()->all();
            foreach ($resumes as  $value) {
                $resumeId [] = $value->id;
            }
        }  

    
        if( Yii::$app->user->identity->type != 0){
            $query = Resume::find()->joinWith('questionary', true)
                ->where(['questionary.user_id' => Yii::$app->user->identity->id ])
                ->andWhere(['resume.id' => $resumeId])->orWhere(['resume.user_id' => Yii::$app->user->identity->id])->orderBy(['is_new' => SORT_DESC]);
        }
        else {
            $query = Resume::find()->where(['id' => $resumeId])->orderBy(['is_new' => SORT_DESC]);
        }

        if(Yii::$app->session['group_id'] !== null){
            $query->andFilterWhere([
                'resume.group_id' => Yii::$app->session['group_id']
            ]);
        }
        
        if(Yii::$app->session['status_id'] !== null){
            $query->andFilterWhere([
                'resume.status_id' => Yii::$app->session['status_id']
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'pagination' => array('pageSize' => 20),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        //$this->joinWith('questionary');
        $query->andFilterWhere([
            'id' => $this->id,
            'questionary_id' => $this->questionary_id,
            'correspondence' => $this->correspondence,
            'group_id' => $this->group_id,
            'status_id' => $this->status_id,
            'date_cr' => $this->date_cr,
            'category' => $this->category,
            'mark' => $this->mark,
            'new_sms' => $this->new_sms,
            'vacancy_id' => $this->vacancy_id,
            'balls' => $this->balls,
            'ball_for_question' => $this->ball_for_question,
            'ip' => $this->ip,
            'user_id' => $this->user_id,
            'buyed' => $this->buyed,
            'doptest' => $this->doptest,
        ]);

        if($questionaryId != null){
            $query->andFilterWhere([
                'questionary_id' => $questionaryId,
            ]);            
        }

      $query->andFilterWhere(['like', 'fit', $this->fit])
            ->andFilterWhere(['like', 'values', $this->values])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'is_new', $this->is_new])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'telegram_chat_id', $this->telegram_chat_id])
            ->andFilterWhere(['like', 'connect_telegram', $this->connect_telegram]);

        return $dataProvider;
    }

    public function searchShop($params, $questionaryId, $post)
    {
        $resumeId = [];
        $check = 0;
        $session = Yii::$app->session;
        if(isset($_GET['page']) && $post['name'] === null) $post['name'] = $session['name'];

        if($post != null && isset($post['name']) ){
            $session['name'] = $post['name'];
            
            $yesNo = Resume::validateTrue($post['name']);
            $allResume = Resume::find()
                ->joinWith('questionary', true)
                ->joinWith('group', true)
                ->joinWith('status', true)
                ->joinWith('vacancy',true)
                ->andWhere(
                    ['or',
                       ['LIKE', 'resume.fio', $post['name']],
                       ['LIKE', 'questionary.name', $post['name']],
                       ['LIKE', 'group.name', $post['name']],
                       ['LIKE', 'resume_status.name', $post['name']],
                       ['LIKE', 'vacancy.name', $post['name']],
                       ['LIKE', 'resume.mark', $post['name']],
                       ['LIKE', 'resume.ball_for_question', $post['name']],
                       ['LIKE', 'resume.code', $post['name']],
                       ['LIKE', 'resume.correspondence', $post['name']],
                       ['LIKE', 'resume.telegram_chat_id', $post['name']],
                       ['LIKE', 'resume.category', $post['name']],
                       ['LIKE', 'resume.date_cr', date('Y-m-d H:i:s', strtotime($post['name']) )],
                       ['LIKE', 'resume.fit', $yesNo],
                       ['LIKE', 'resume.new_sms', $yesNo],
                       ['LIKE', 'resume.connect_telegram', $yesNo],
                       ['LIKE', 'resume.is_new', $yesNo],
                    ])
                ->all();

            foreach ($allResume as  $value) {
                $resumeId [] = $value->id;
            } 


            $resumes = Resume::find()->all();
            foreach ($resumes as $model) {
                foreach (json_decode($model->tags) as $value) {
                    $tag = Tags::find()->where(['LIKE','name', $post['name'] ])->andWhere(['id' => $value->id ])->one();
                    if($tag != null) $resumeId [] = $model->id;
                }
            }

            foreach ($resumes as $model) {
                foreach (json_decode($model->values) as $value) {
                    if( strpos( $value->value, json_encode($post['name']) ) !== false) $resumeId [] = $model->id;
                    if( strpos( $value->value, $post['name'] ) !== false) $resumeId [] = $model->id;
                }
            }
        }
        else{
            $resumes = Resume::find()->all();
            foreach ($resumes as  $value) {
                $resumeId [] = $value->id;
            }
        }  

        /*echo "<pre>";
        print_r($resumeId);
        echo "</pre>";
        die;*/
        
        $query = Resume::find()
            ->where(['id' => $resumeId])
            ->andWhere(['or',
               ['show_in_shop'=>0],
               ['show_in_shop'=>null]
           ])
            ->orderBy(['is_new' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'pagination' => array('pageSize' => 20),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        //$this->joinWith('questionary');
        $query->andFilterWhere([
            'id' => $this->id,
            'questionary_id' => $this->questionary_id,
            'correspondence' => $this->correspondence,
            'group_id' => $this->group_id,
            'status_id' => $this->status_id,
            'date_cr' => $this->date_cr,
            'category' => $this->category,
            'mark' => $this->mark,
            'new_sms' => $this->new_sms,
            'vacancy_id' => $this->vacancy_id,
            'balls' => $this->balls,
            'ball_for_question' => $this->ball_for_question,
            'ip' => $this->ip,
            'user_id' => $this->user_id,
            'buyed' => $this->buyed,
            'doptest' => $this->doptest,
        ]);

        if($questionaryId != null){
            $query->andFilterWhere([
                'questionary_id' => $questionaryId,
            ]);            
        }

      $query->andFilterWhere(['like', 'fit', $this->fit])
            ->andFilterWhere(['like', 'values', $this->values])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'is_new', $this->is_new])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'telegram_chat_id', $this->telegram_chat_id])
            ->andFilterWhere(['like', 'connect_telegram', $this->connect_telegram]);

        return $dataProvider;
    }
}
