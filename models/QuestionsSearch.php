<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Questions;

/**
 * QuestionsSearch represents the model behind the search form about `app\models\Questions`.
 */
class QuestionsSearch extends Questions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'questionary_id', 'ordering', 'view_in_table', 'type', 'general_access', 'own_option'], 'integer'],
            [['name', 'description', 'foto', 'require', 'text', 'date', 'individual', 'multiple'], 'safe'],
            [['number'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $query = Questions::find()->where(['questionary_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['ordering'=>SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'questionary_id' => $this->questionary_id,
            'ordering' => $this->ordering,
            'view_in_table' => $this->view_in_table,
            'type' => $this->type,
            'number' => $this->number,
            'date' => $this->date,
            'general_access' => $this->general_access,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'foto', $this->foto])
            ->andFilterWhere(['like', 'require', $this->require])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'individual', $this->individual])
            ->andFilterWhere(['like', 'multiple', $this->multiple]);

        return $dataProvider;
    }
}
