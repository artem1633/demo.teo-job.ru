<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PaymentOrders;

/**
 * PaymentOrdersSearch represents the model behind the search form about `app\models\PaymentOrders`.
 */
class PaymentOrdersSearch extends PaymentOrders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'type', 'company_id'], 'integer'],
            [['date_order', 'date_payment', 'requisites', 'description'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentOrders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date_order' => $this->date_order,
            'date_payment' => $this->date_payment,
            'amount' => $this->amount,
            'status' => $this->status,
            'type' => $this->type,
            'company_id' => $this->company_id,
        ]);

        $query->andFilterWhere(['like', 'requisites', $this->requisites])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
