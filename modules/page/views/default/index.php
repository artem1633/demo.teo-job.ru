<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Users;

?> 
<div class="row">
    <div class="col-md-12 ">
        <h1  class="company">  
            <span><?= $user->fio ?></span>
        </h1>
            <br><br>
            <div style="padding-left: 150px;">
                <?= $user->description ?>
            </div>
    </div>
</div>

<div class="row">
    <?php
        foreach ($questionary as $value) {
    ?> 
        <div class="col-md-3" style="margin-top: 10px;">
            <div class="panel panel-success top-non">
                <div class="panel panel-success">
                            <div class="template-name ">
                                <div class="col-md-9">
                                    <h4><?= $value->name; ?></h4>
                                </div>
                                <div class="col-md-3" style="padding-top:10px;">
                                    <a class="btn btn-danger" href="<?='https://' . $_SERVER['SERVER_NAME'] . '/' . $value->link;?>" title="Перейти" >Перейти </a>
                                </div>
                            </div>

                    <div class="panel-body">
                        <div class="desc-info">
                            <?= $value->description; ?>
                        </div>
                    </div>                                
                </div>
            </div>
        </div>
  <?php } ?> 
</div>
