<?php

use yii\db\Migration;

/**
 * Handles adding ball_for_question to table `questionary`.
 */
class m181221_132305_add_ball_for_question_column_to_questionary_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questionary', 'ball_for_question', $this->integer()->comment('Баллы за текстовые ответы'));
        $this->addColumn('resume', 'balls', $this->integer()->comment('Баллы за ответы'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questionary', 'ball_for_question');
        $this->dropColumn('resume', 'balls');
    }
}
