<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m180714_041755_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'key' => $this->string(255),
            'value' => $this->string(500),
        ]);

        $this->insert('settings',array(
            'name' => 'Телеграмм бот для оповещение',
            'key' => 'telegram_bot_for_alert',
            'value' =>'',
        ));

        $this->insert('settings',array(
            'name' => 'Телеграмм бот для общения с резюме',
            'key' => 'telegram_bot_for_resume',
            'value' =>'',
        ));

        $this->insert('settings',array(
            'name' => 'Текст после заполнения анкеты',
            'key' => 'text_after_filling',
            'value' =>'',
        ));

        $this->insert('settings',array(
            'name' => 'Инструкция редактор',
            'key' => 'instruction_editor',
            'value' =>'',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }
}
