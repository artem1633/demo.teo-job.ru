<?php

use yii\db\Migration;

/**
 * Handles dropping special from table `resume`.
 */
class m181112_011721_drop_special_column_from_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('resume', 'special');
        $this->addColumn('resume', 'vacancy_id', $this->integer());
        $this->addColumn('resume', 'values', $this->text());

        $this->createIndex('idx-resume-vacancy_id', 'resume', 'vacancy_id', false);
        $this->addForeignKey("fk-resume-vacancy_id", "resume", "vacancy_id", "vacancy", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('resume', 'special', $this->string(255));

        $this->dropForeignKey('fk-resume-vacancy_id','resume');
        $this->dropIndex('idx-resume-vacancy_id','resume');

        $this->dropColumn('resume', 'vacancy_id');
        $this->dropColumn('resume', 'values');
    }
}
