<?php

use yii\db\Migration;

/**
 * Handles the creation of table `chat`.
 */
class m181115_122953_create_chat_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('chat', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->string(255),
            'user_id' => $this->integer(),
            'text' => $this->text(),
            'date_time' => $this->datetime(),
            'is_read' => $this->boolean(),
        ]);

        $this->createIndex('idx-chat-user_id', 'chat', 'user_id', false);
        $this->addForeignKey("fk-chat-user_id", "chat", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-chat-user_id','chat');
        $this->dropIndex('idx-chat-user_id','chat');

        $this->dropTable('chat');
    }
}
