<?php

use yii\db\Migration;

/**
 * Handles adding is_template to table `questionary`.
 */
class m181116_082954_add_is_template_column_to_questionary_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questionary', 'is_template', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questionary', 'is_template');
    }
}
