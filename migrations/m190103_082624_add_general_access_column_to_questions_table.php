<?php

use yii\db\Migration;

/**
 * Handles adding general_access to table `questions`.
 */
class m190103_082624_add_general_access_column_to_questions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questions', 'general_access', $this->boolean()->comment('Показывать это поле в общем доступе'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questions', 'general_access');
    }
}
