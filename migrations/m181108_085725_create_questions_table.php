<?php

use yii\db\Migration;

/**
 * Handles the creation of table `questions`.
 */
class m181108_085725_create_questions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('questions', [
            'id' => $this->primaryKey(),
            'questionary_id' => $this->integer()->comment('Анкета'),
            'name' => $this->string(255)->comment('Наименование'),
            'description' => $this->text()->comment('Описание'),
            'foto' => $this->text()->comment('Картинка'),
            'require' => $this->boolean()->comment('Обязательное или нет'),
            'ordering' => $this->integer()->comment('Сортировка'),
            'view_in_table' => $this->integer()->comment('Выводить в таблицу информацию'),
            'type' => $this->integer()->comment('Тип вопроса'),
            'text' => $this->text()->comment('Текст'),
            'number' => $this->float()->comment('Число'),
            'date' => $this->date()->comment('Дата'),
            'individual' => $this->text()->comment('Выбор одного варианта'),
            'multiple' => $this->text()->comment('Выбор несколько вариантов'),
        ]);

        $this->createIndex('idx-questions-questionary_id', 'questions', 'questionary_id', false);
        $this->addForeignKey("fk-questions-questionary_id", "questions", "questionary_id", "questionary", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-questions-questionary_id','questions');
        $this->dropIndex('idx-questions-questionary_id','questions');
        
        $this->dropTable('questions');
    }
}
