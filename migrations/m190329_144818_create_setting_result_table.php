<?php

use yii\db\Migration;

/**
 * Handles the creation of table `setting_result`.
 */
class m190329_144818_create_setting_result_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('setting_result', [
            'id' => $this->primaryKey(),
            'questionary_id' => $this->integer()->comment('Анкета/Тест'),
            'condition' => $this->integer()->comment('Условия'),
            'first_value' => $this->float()->comment('Значения1'),
            'second_value' => $this->float()->comment('Значения2'),
            'text' => $this->text()->comment('Описания'),
        ]);

        $this->createIndex('idx-setting_result-questionary_id', 'setting_result', 'questionary_id', false);
        $this->addForeignKey("fk-setting_result-questionary_id", "setting_result", "questionary_id", "questionary", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-setting_result-questionary_id','setting_result');
        $this->dropIndex('idx-setting_result-questionary_id','setting_result');
        
        $this->dropTable('setting_result');
    }
}
