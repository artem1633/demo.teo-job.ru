<?php

use yii\db\Migration;

/**
 * Handles adding tags to table `alert`.
 */
class m181119_042751_add_tags_column_to_alert_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('alert', 'tags', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('alert', 'tags');
    }
}
