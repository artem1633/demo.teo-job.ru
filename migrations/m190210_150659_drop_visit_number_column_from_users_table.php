<?php

use yii\db\Migration;

/**
 * Handles dropping visit_number from table `users`.
 */
class m190210_150659_drop_visit_number_column_from_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('users', 'visit_number');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('users', 'visit_number', $this->integer());
    }
}
