<?php

use yii\db\Migration;

/**
 * Handles dropping time_test from table `questionary`.
 */
class m190119_113237_drop_time_test_column_from_questionary_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('questionary', 'time_test');

        $this->addColumn('questionary', 'time_test', $this->integer()->comment('Время для теста'));
        $this->addColumn('resume', 'time_spent', $this->integer()->comment('Время которое ушло на тест'));
        $this->addColumn('resume', 'delivered', $this->integer()->comment('Сдан или нет'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questionary', 'time_test');

        $this->addColumn('questionary', 'time_test', $this->time()->comment('Время для теста'));
        $this->dropColumn('resume', 'time_spent');
        $this->dropColumn('resume', 'delivered');
    }
}
