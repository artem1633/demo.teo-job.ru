<?php

use yii\db\Migration;

/**
 * Handles adding type to table `questionary`.
 */
class m190116_094525_add_type_column_to_questionary_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questionary', 'type', $this->integer()->comment('Тип'));
        $this->addColumn('questionary', 'passage_ball', $this->float()->comment('Проходной бал'));
        $this->addColumn('questionary', 'time_test', $this->time()->comment('Время '));

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT * FROM questionary");
        $questionaries = $command->queryAll();
        foreach ($questionaries as $questionary) 
        {
            Yii::$app->db->createCommand()->update('questionary', ['type' => 1], [ 'id' => $questionary['id'] ])->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questionary', 'type');
        $this->dropColumn('questionary', 'passage_ball');
        $this->dropColumn('questionary', 'time_test');
    }
}
