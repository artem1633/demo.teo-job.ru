<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lessons_users}}`.
 */
class m190209_094830_create_lessons_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lessons_users}}', [
            'id' => $this->primaryKey(),
            'lessons_group' => $this->integer()->comment('Номер групи'),
            'user_id'=>$this->integer()->comment('Пользователь'),
            'passed' => $this->integer()->comment('Пройдено')->defaultValue(0)
        ]);
        $this->createIndex('idx-lessons_users-user_id', '{{%lessons_users}}', 'user_id', false);
        $this->addForeignKey("fk-lessons_users-user_id", "{{%lessons_users}}", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lessons_users}}');
    }
}
