<?php

use yii\db\Migration;

/**
 * Handles adding result_resume_sum to table `questionary`.
 */
class m190226_045515_add_result_resume_sum_column_to_questionary_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questionary', 'result_resume_sum', $this->float()->comment('Стоимость одного результата'));
        $this->addColumn('resume', 'sales', $this->float()->comment('Продано'));
        $this->addColumn('resume', 'show_in_shop', $this->boolean()->comment('Не отоброжать в магазине'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questionary', 'result_resume_sum');
        $this->dropColumn('resume', 'sales');
        $this->dropColumn('resume', 'show_in_shop');
    }
}
