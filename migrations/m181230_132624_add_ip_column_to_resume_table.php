<?php

use yii\db\Migration;

/**
 * Handles adding ip to table `resume`.
 */
class m181230_132624_add_ip_column_to_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('resume', 'ip', $this->string(255)->comment('IP адрес'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('resume', 'ip');
    }
}
