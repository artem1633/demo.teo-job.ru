<?php

use yii\db\Migration;

/**
 * Handles adding main_balanve to table `users`.
 */
class m190117_162734_add_main_balanve_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'main_balance', $this->float()->comment('Баланс основной'));
        $this->addColumn('users', 'partner_balance', $this->float()->comment('Баланс партнерский'));
        $this->addColumn('users', 'referal_id', $this->integer()->comment('Реферал'));
        $this->addColumn('users', 'prosent_referal', $this->float()->comment('Процент с оплат рефералов'));
        $this->addColumn('users', 'questionary_sum', $this->float()->comment('Стоимость 1 анкеты'));
        $this->addColumn('users', 'resume_sum', $this->float()->comment('Стоимость 1 резюме'));

        $this->insert('settings',array(
            'name' => 'Процент с оплат рефералов',
            'key' => 'referal_pay',
            'text' =>'0',
        ));

        $this->insert('settings',array(
            'name' => 'Стоимость 1 анкеты',
            'key' => 'questionary_cost',
            'text' =>'0',
        ));

        $this->insert('settings',array(
            'name' => 'Стоимость 1 резюме',
            'key' => 'resume_cost',
            'text' =>'0',
        ));

        $this->insert('settings',array(
            'name' => 'Бонус за регистрацию',
            'key' => 'registr_bonus',
            'text' =>'0',
        ));

        $this->insert('settings',array(
            'name' => 'Условия партнерки',
            'key' => 'affiliate_terms',
            'text' =>'',
        ));

        $this->insert('settings',array(
            'name' => 'Ключ от кошелька яндекса',
            'key' => 'key_yandex_kesh',
            'text' =>'',
        ));

        $this->insert('settings',array(
            'name' => 'Номер кошелека',
            'key' => 'cash_number',
            'text' =>'',
        ));
    }

    /**    }

     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'main_balance');
        $this->dropColumn('users', 'partner_balance');
        $this->dropColumn('users', 'referal_id');
        $this->dropColumn('users', 'prosent_referal');
        $this->dropColumn('users', 'questionary_sum');
        $this->dropColumn('users', 'resume_sum');
    }
}
