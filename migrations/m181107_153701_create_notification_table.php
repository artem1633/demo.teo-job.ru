<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notification`.
 */
class m181107_153701_create_notification_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('notification', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'key' => $this->string(255),
            'value' => $this->text(),
            'user_id' => $this->integer(),
        ]);

        $this->createIndex('idx-notification-user_id', 'notification', 'user_id', false);
        $this->addForeignKey("fk-notification-user_id", "notification", "user_id", "users", "id");

        $this->insert('notification',array(
            'name' => 'При новом посещение',
            'key' => 'visiting',
            'value' =>'',
            'user_id' => 1,
        ));

        $this->insert('notification',array(
            'name' => 'При новом заполнении',
            'key' => 'filling',
            'value' =>'',
            'user_id' => 1,
        ));

        $this->insert('notification',array(
            'name' => 'Новости и акция',
            'key' => 'news_and_action',
            'value' =>'',
            'user_id' => 1,
        ));

        $this->insert('notification',array(
            'name' => 'Сменили группу',
            'key' => 'change_group',
            'value' =>'',
            'user_id' => 1,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-notification-user_id','notification');
        $this->dropIndex('idx-notification-user_id','notification');
        
        $this->dropTable('notification');
    }
}
