<?php

namespace app\controllers;

use app\modules\api\controllers\BotinfoController;
use Yii;
use app\models\Questionary;
use app\models\QuestionarySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\QuestionsSearch;
use app\models\Questions;
use app\models\Alert;
use kartik\mpdf\Pdf;
use app\models\SettingResultSearch;
use app\models\SettingResult;

/**
 * QuestionaryController implements the CRUD actions for Questionary model.
 */
class QuestionaryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Questionary models.
     * @return mixed
     */
    public function actionIndex($is_template = null)
    {    
        $searchModel = new QuestionarySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $is_template);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'is_template' => $is_template
        ]);
    }

    /**
     * Lists all Questionary template models.
     * @return mixed
     */
    public function actionTemplate($is_template = 1)
    {    
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT * FROM questionary WHERE is_template = 1");
        $templates = $command->queryAll();

        return $this->render('template', [
            'templates' => $templates,
            'is_template' => $is_template
        ]);
    }

    public function actionSetStatus($id)
    {
        $questionary = Questionary::findOne($id);
        $questionary->show_in_desktop = 0;
        $questionary->save();
    }

    //Вкладка "Страница"
    public function actionQuestions($id)
    {    
        $request = Yii::$app->request;
        if($request->post()) {

            $post = $request->post();
            $active = $post['active'];
            $strlen = strlen( $active );
            $question_id = ''; $order_number = 0;
            for( $i = 0; $i <= $strlen; $i++ ) {
                $char = substr( $active, $i, 1 );             
                if($char == ',') {
                    $order_number++;
                    Yii::$app->db->createCommand()->update('questions', 
                        [ 'ordering' => $order_number ], 
                        [ 'id' => $question_id ])
                    ->execute();
                    $question_id = '';
                }
                else $question_id .= $char;
            }
            $order_number++;
            Yii::$app->db->createCommand()->update('questions', 
                [ 'ordering' => $order_number ], 
                [ 'id' => $question_id ])
            ->execute();
            Yii::$app->session->setFlash('success', "Успешно выполнено");
            return $this->redirect(['questions', 'id' => $id]);
        }

        $questionary = Questionary::findOne($id);
        $active = [];
        if($questionary != null) {
            $questions = Questions::find()->where(['questionary_id' => $id])->orderBy([ 'ordering' => SORT_ASC])->all();
            foreach ($questions as $question) {
                $name = $question->getItemValues();
                $active += [
                    $question->id => ['content' => $name],
                ];
            }
        }
        
        $alerts = Alert::find()->where(['questionary_id' => $id])->all();
        $searchModel = new SettingResultSearch();
        $dataProviderSetting = $searchModel->search(Yii::$app->request->queryParams,$id);

        return $this->render('questions', [
            'questionary' => $questionary,
            'id' => $id,
            'active' => $active,
            'alerts' => $alerts,
            'dataProviderSetting' => $dataProviderSetting,
        ]);
    }


    /**
     * Displays a single Questionary model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate($active = 0, $is_template = null)
    {
        $request = Yii::$app->request;
        $model = new Questionary();
        $model->active = $active;
        $model->is_template = $is_template;
        $post = $request->post();
        Yii::$app->response->format = Response::FORMAT_JSON;

        if($post['Questionary']['step'] == 2)
        {
            if($model->load($request->post()) && $model->validate() && $model->save())
            {
                if($model->is_template == 1){
                    Yii::$app->db->createCommand()->update('questionary', 
                        [ 'user_id' => null ], 
                        [ 'id' => $model->id ])
                    ->execute();
                }
                if($request->post()['Questionary']['template'] != null)
                {
                    $template_id = $request->post()['Questionary']['template'];
                    $questions = Questions::find()
                        ->where(['questionary_id' => $template_id ])
                            ->andWhere(['!=', 'type', 0])
                            ->all();
                        foreach ($questions as $value) {
                            $question = new Questions();
                            $question->attributes = $value->attributes;
                            $question->questionary_id = $model->id;
                            $question->save();
                        }
                    }
                    $text = 'Новая анкета в системе';
                    if ($model->name) $text .= "\n Название: ".$model->name;
                    if ($model->user_id) $text .= "\n Компания: ".$model->user_id;
                    BotinfoController::sendadmin(['chat_id' => '247187885', 'parse_mode'=>'HTML', 'text' => $text]);
                    return $this->redirect(['questions', 'id' => $model->id]);
            }
            $model->step = 2;
        }
        $t = '';
        if($post['Questionary']['step'] == 1)
        {
            $model->step = 1;
            $t .= '1';
            if($model->load($request->post()) && $model->validate())
            {
                $t .= '2';
                $model->step = 2;
            }
        }

        if($post['Questionary']['step'] == null)
        {
            $t .= '1';
            $model->step = 1;
        }

        return [
            'title'=> 'Создать',
            'size' => 'large',
            'content'=>$this->renderAjax('create', [
                   'model' => $model,
            ]),
            'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button($model->step == 2 ? 'Создать' : 'Далее' ,['class'=>'btn btn-info',
                            'type'=>"submit",'data-introindex'=>'3-5'])
        ];
    }

    /**
     * Creates a new Questionary model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCopy($id, $copy_from_template = null)
    {
        $request = Yii::$app->request;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT * FROM questionary WHERE id = " . $id);
        $old = $command->queryOne();

        $model = new Questionary();
        $model->name = $old['name'] . '-копия';
        $model->description = $old['description'];
        $model->vacancy_id = $old['vacancy_id'];
        $model->access = $old['access'];
        $model->show_in_desktop = $old['show_in_desktop'];
        $model->publish_company = $old['publish_company'];
        $model->count = 0;
        if($copy_from_template == 1) $model->is_template = null;
        else $model->is_template = $old['is_template'];
        $model->active = 1;

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->validate() && $model->save()){
            $questions = Questions::find()->where(['questionary_id' => $old['id']])->andWhere(['!=', 'type', 0])->all();
            foreach ($questions as $value) {
                $question = new Questions();
                $question->attributes = $value->attributes;
                $question->questionary_id = $model->id;
                $question->save();
            }
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'title'=> "Анкеты",
                'size' => 'normal',
                'content'=>'<span class="text-success">Успешно выполнено</span>',
            ];
        }
    }

    /**
     * Updates an existing Questionary model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id); 
        $post = $request->post();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#questionary-update-pjax',
                    'title'=> "Анкета",
                    "size" => 'large',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    "size" => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Questionary model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Questionary model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Questionary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Questionary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Questionary::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPrint($id)
    {
        $questionary = Questionary::findOne($id);
        if($questionary != null) {
            $questions = Questions::find()->where(['questionary_id' => $id])->orderBy([ 'ordering' => SORT_ASC])->all();
        }
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        $this->layout='print';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $this->render('print',['questions'=>$questions,'questionary'=>$questionary]),
            'cssFile' => 'css/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.img-circle {border-radius: 50%;}',
            'options' => [
                'title' => 'Teo-JOB',
                'subject' => 'PDF'
            ],
            'methods' => [
                'SetHeader'=>['Сделано с помощью сервиса https://teo-job.ru'],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
        return $pdf->render();

    }

    public function actionAddCondition($id)
    {
        $request = Yii::$app->request;
        $model = new SettingResult(); 
        $model->questionary_id = $id; 

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            return [
                'forceReload'=>'#conditions-pjax',
                'title'=> "Настройка результатов",
                'forceClose'=>true,
            ];         
        }else{           
            return [
                'title'=> "Настройка результатов",
                'content'=>$this->renderAjax('add-condition', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
            ];         
        }
    }

    public function actionUpdateCondition($id)
    {
        $request = Yii::$app->request;
        $model = SettingResult::findOne($id);

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            return [
                'forceReload'=>'#conditions-pjax',
                'title'=> "Настройка результатов",
                'forceClose'=>true,
            ];         
        }else{           
            return [
                'title'=> "Настройка результатов",
                'content'=>$this->renderAjax('add-condition', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
            ];         
        }
    }

    public function actionDeleteCondition($id)
    {
        $request = Yii::$app->request;
        SettingResult::findOne($id)->delete();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#conditions-pjax'];
    }

}
