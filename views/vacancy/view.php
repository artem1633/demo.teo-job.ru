<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Vacancy */
?>
<div class="vacancy-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
