<?php
use yii\helpers\Url;
use app\models\Questionary;
use yii\helpers\ArrayHelper;
use yii\helpers\Arrai;
use app\models\Group;
use app\models\ResumeStatus;
use app\models\Vacancy;
use app\models\Tags;
use yii\helpers\Html;
use app\models\Resume;
use app\models\Questions;
use kartik\select2\Select2;
use kartik\grid\GridView;



return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'kartik\grid\ExpandRowColumn',
        'width'=>'50px',
        'contentOptions' =>['data-introindex' => "8-12"],
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($model, $key, $index, $column) {
            return \Yii::$app->controller->renderPartial('_questions', ['model'=>$model]);
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => "avatar",
        'width' =>'60px',
        'content' => function ($data) {
            if($data->avatar == null) $path = 'https://' . $_SERVER['SERVER_NAME'] . '/images/nouser.png';
            else $path = 'https://' . $_SERVER['SERVER_NAME'] . '/' . $data->avatar;
            $name = $data->questionary->name;
            $name = substr($name,0,20);
            return '<div class="example2">
                        <img src="'. $path .' " class="example_beauty" style="width: 60px;border-radius: 1em;border: solid 1px #cecece;">
                        <span>'.$name.'...</span>
                    </div>';
            return '<img style="width: 60px;border-radius: 1em;border: solid 1px #cecece;" src="' . $path . '" >';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
        //'width' => '350px',
        'header' => '<div style="min-width:500px;">ФИО / Балы</div>',
        //'header' => 'ФИО / Балы',
        'content' => function($data){
            if($data->connect_telegram == 1) $icon = ' <span class="fa fa-envelope-o"></span>';
            else $icon = '';
            if($data->is_new) $is_new = ' <span style="color:red;" data-introindex="8-3">new</span> ';
            else $is_new = '<span data-introindex="8-3">&nbsp;</span> ';

            $doptest = '';
            if ($data->doptest) {
                $url = Url::to(['/resume/view', 'id' => $data->doptest]);
                $doptest = Html::a('<button class="btn btn-info btn-xs"><span class="fa fa-code-fork"></span></button>', $url, ['title'=>'Перейти в основную анкету','target' => '_blank',  'data-toggle'=>'tooltip']);
            }

            $result = [];
            foreach (json_decode($data->tags) as $value) {
                $result [] = $value->id;
            }
            $url = Url::to(['/resume/add-resume', 'id' => $data->id]);
            $add = Html::a('<button class="btn btn-info btn-xs"><span class="fa fa-magic"></span></button>', $url, ['role'=>'modal-remote','title'=>'Предложить тест', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/resume/print', 'id' => $data->id]);
            $print = Html::a('<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-print"></span></button>', $url, ['data-pjax'=>'0','title'=>'Печать', 'target' => '_blank', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/resume/view', 'id' => $data->id]);
            $update = Html::a('<button class="btn btn-info btn-xs" data-introindex="8-6"><span class="glyphicon glyphicon-eye-open"></span></button>', $url, ['data-pjax'=>'0','title'=>'Изменить', 'target' => '_blank', 'data-toggle'=>'tooltip']);

            //$url = Url::to(['/resume/view', 'id' => $data->id]);
            $share = Html::a('<button class="btn btn-warning btn-xs" data-introindex="8-4"><span class="glyphicon glyphicon-share"></span></button>', ['/'.$data->code], ['data-pjax' => '0','title'=>'Поделиться','target'=> "_blank", 'data-toggle'=>'tooltip']);

            $que = Html::a('<button class="btn btn-primary btn-xs" data-introindex="8-5"><span class="glyphicon glyphicon-file"></span></button>', ['/questionary/questions', 'id' => $data->questionary_id],
                ['data-pjax'=>'0','title'=> 'Открыть Анкету '.$data->questionary->name, 'target' => '_blank' , 'data-toggle'=>'tooltip']);
            $url = Url::to(['/resume/delete', 'id' => $data->id]);
            $delete = Html::a('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>', $url,
                [
                    'role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Подтвердите действие',
                    'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                ]);
            $star = '';
            for ($i1 = 0; $i1 < $data->mark; $i1++){
                $star .= "</span> <i class='glyphicon glyphicon-star'></i>";
            }
            $sales = '';
            if($data->sales !== null) $sales = '<span class="fa fa-ruble"></span> '. $data->sales . '&nbsp; ';
            $url = Url::to(['/resume/view', 'id' => $data->id]);
            return  Html::a('<button class="btn btn-info btn-xs" data-introindex="8-1">'.$data->fio.'</button>',
                    $url, ['data-pjax'=>'0','title'=>'Просмотр', 'target' => '_blank', 'data-toggle'=>'tooltip']) . $icon .
                "  <i class='glyphicon glyphicon-signal' data-introindex=\"8-2\"></i> <span class='success'  >".($data->balls + $data->ball_for_question)."   {$star} ".
                $is_new . '<span class="pull-right">'. $sales. '&nbsp;' . $add . '&nbsp;' . $doptest . '&nbsp;' . $print . '&nbsp;'. $update . '&nbsp; '. $share . '&nbsp; '. $que . '&nbsp; ' . $delete .'</span>  <br/>
                    
                    <div class="col-12">
                    <div style="margin-top:10px; padding-left:0px;" data-introindex="8-7" class="col-md-6">' .
                Select2::widget([
                    'name' => 'tags',
                    'data' => Yii::$app->session['tags'],
                    'value' => $result,
                    'size' => 'sm',
                    'options' => [ 'id' => 'tags' . $data->id,'placeholder' => 'Укажите теги ...'],
                    'pluginOptions' => [
                        'tags' => true,
                        'allowClear' => true,
                        'multiple' => true,
                    ],
                    'pluginEvents' => [
                        "change" => "function() 
                        {
                            $.get('/resume/set-tags',
                            { 'val' : JSON.stringify($(this).val()), 'id' : $(this).attr('id') },
                                function(data){}
                            );
                        }",
                    ],
                ]) .'</div>
                <div style="margin-top:10px; padding-right:0px;" data-introindex="8-8" class="col-md-3">' . Select2::widget([
                    'name' => 'group',
                    'data' => Yii::$app->session['group'],
                    'value' => $data->group_id,
                    'size' => 'sm',
                    'options' => [ 'id' => 'group' . $data->id,'placeholder' => 'Выберите группу...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                    'pluginEvents' => [
                        "change" => "function() 
                    {                                        
                        $.get('/resume/set-group',
                        { 'val' : $(this).val(), 'id' : $(this).attr('id') },
                            function(data){ }
                        );
                    }",
                    ],
                ]) . '</div>
                
                '.
                '<div style="margin-top:10px; padding-right:0px;" data-introindex="8-8" class="col-md-3">' . Select2::widget([
                    'name' => 'status',
                    'data' => Yii::$app->session['status'],
                    'value' => $data->status_id,
                    'size' => 'sm',
                    'options' => [ 'id' => 'status' . $data->id,'placeholder' => 'Выберите статус...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                    'pluginEvents' => [
                        "change" => "function() 
                    {                                        
                        $.get('/resume/set-status',
                        { 'val' : $(this).val(), 'id' : $(this).attr('id') },
                            function(data){ }
                        );
                    }",
                    ],
                ]) . '</div>
                
                </div>';
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'group_id',
//        'width' => '270px',
//        'filter' => ArrayHelper::map(Group::find()->all(), 'id', 'name'),
//        'content' => function($data){
//            return '<div style="margin-top:32px;" data-introindex="8-8">' . Select2::widget([
//                    'name' => 'group',
//                    'data' => Yii::$app->session['group'],
//                    'value' => $data->group_id,
//                    'size' => 'sm',
//                    'options' => [ 'id' => 'group' . $data->id,'placeholder' => 'Выберите ...'],
//                    'pluginOptions' => [
//                        'allowClear' => true,
//                    ],
//                    'pluginEvents' => [
//                        "change" => "function()
//                    {
//                        $.get('/resume/set-group',
//                        { 'val' : $(this).val(), 'id' : $(this).attr('id') },
//                            function(data){ }
//                        );
//                    }",
//                    ],
//                ]) . '</div>';
//        }
//    ],

//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'status_id',
//        'width' => '270px',
//        'filter' => ArrayHelper::map(ResumeStatus::find()->all(), 'id', 'name'),
//        'content' => function($data){
//            return '<div style="margin-top:32px;" data-introindex="8-9">' . Select2::widget([
//                    'name' => 'status',
//                    'data' => Yii::$app->session['status'],
//                    'value' => $data->status_id,
//                    'size' => 'sm',
//                    'options' => [ 'id' => 'status' . $data->id,'placeholder' => 'Выберите ...'],
//                    'pluginOptions' => [
//                        'allowClear' => true,
//                    ],
//                    'pluginEvents' => [
//                        "change" => "function()
//                    {
//                        $.get('/resume/set-status',
//                        { 'val' : $(this).val(), 'id' : $(this).attr('id') },
//                            function(data){ }
//                        );
//                    }",
//                    ],
//                ]) . '</div>';
//        }
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'questionary_id',
//        'header' => 'Анкета / Поделить резюме',
//        'filter' => ArrayHelper::map(Questionary::find()->all(), 'id', 'name'),
//        'content' => function($data){
//            return '<br/>' . Html::a($data->questionary->name, ['/questionary/questions', 'id' => $data->questionary_id],
//                ['data-pjax'=>'0','title'=> 'Редактировать', 'target' => '_blank' ]) . '<p></p>' . Html::a('<span class="is-hidden-mobile">https://'. $_SERVER['SERVER_NAME'].'/'.$data->code .'  </span>', ['/'.$data->code], ['data-pjax' => '0','target'=> "_blank"]);
//        }
//    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'code',
        'content' => function($data){
            return '<div style="margin-top:32px;">' . Html::a('<span class="is-hidden-mobile">https://'. $_SERVER['SERVER_NAME'].'/'.$data->code .'  </span>', ['/'.$data->code], ['data-pjax' => '0','target'=> "_blank"]) . '</div>';
        }
    ],*/
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'ip',
    //     'visible' => Yii::$app->user->identity->type == 0 ? true : false,
    // ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'vacancy_id',
        'filter' => ArrayHelper::map(Vacancy::find()->all(), 'id', 'name'),
        'content' => function($data){
            return $data->vacancy->name;
        }
    ],*/
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tags',
        'width' => '200px',
        'content' => function($data){
            $result = [];
            foreach (json_decode($data->tags) as $value) {
                $result [] = $value->id;
            }            // return $result;
            return  Select2::widget([
                'name' => 'tags',               
                'data' => Yii::$app->session['tags'],
                'value' => $result,
                'size' => 'sm',
                'options' => [ 'id' => 'tags' . $data->id,'placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                    'multiple' => true,
                ],
                'pluginEvents' => [
                    "change" => "function() 
                    {
                        $.get('/resume/set-tags',
                        { 'val' : JSON.stringify($(this).val()), 'id' : $(this).attr('id') },
                            function(data){}
                        );
                    }",
                ],
            ]); 
        }
    ],*/
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'balls',
        'content' => function($data){
            return $data->balls + $data->ball_for_question;
        }
    ],*/
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'buttons',
        'width'=>'100px',
        'label' => 'Действия',
        'content' => function($data){
            $url = Url::to(['/resume/view', 'id' => $data->id]);
            $update = Html::a('<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>', $url, ['data-pjax'=>'0','title'=>'Изменить', 'target' => '_blank', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/resume/delete', 'id' => $data->id]);
            $delete = Html::a('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>', $url,
                [
                    'role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Подтвердите действие',
                    'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                ]);

            return '<center>' . $update . '&nbsp; ' . $delete . '</center>';
        }
    ],*/
];