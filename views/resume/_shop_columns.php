<?php
use yii\helpers\Url;
use app\models\Questionary;
use yii\helpers\ArrayHelper;
use app\models\Group;
use app\models\ResumeStatus;
use app\models\Vacancy;
use app\models\Tags;
use yii\helpers\Html;
use app\models\Resume;
use app\models\Questions;
use kartik\select2\Select2;
use kartik\grid\GridView;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'kartik\grid\ExpandRowColumn',
        'width'=>'50px',
        'contentOptions' =>['data-introindex' => "8-12"],
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($model, $key, $index, $column) {
            return \Yii::$app->controller->renderPartial('_questions', ['model'=>$model]);
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => "avatar",
        'width' =>'60px',
        'content' => function ($data) {
            if($data->avatar == null) $path = 'https://' . $_SERVER['SERVER_NAME'] . '/images/nouser.png';
            else $path = 'https://' . $_SERVER['SERVER_NAME'] . '/' . $data->avatar;

            return '<img style="width: 60px;border-radius: 1em;border: solid 1px #cecece;" src="' . $path . '" >';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
        'header' => '<div style="min-width:420px;">ФИО / Балы</div>',
        'content' => function($data){
            $sales = $data->questionary->result_resume_sum;
            $result = [];
            foreach (json_decode($data->tags) as $value) {
                $result [] = $value->id;
            }

            $url = Url::to(['/resume/view', 'id' => $data->id]);
            $update = Html::a('<button class="btn btn-info btn-xs" data-introindex="8-6"><span class="glyphicon glyphicon-eye-open"></span></button>', $url, ['data-pjax'=>'0','title'=>'Изменить', 'target' => '_blank', 'data-toggle'=>'tooltip']);

            //$url = Url::to(['/resume/view', 'id' => $data->id]);
            $share = Html::a('<button class="btn btn-warning btn-xs" data-introindex="8-4"><span class="glyphicon glyphicon-share"></span></button>', ['/'.$data->code], ['data-pjax' => '0','title'=>'Поделиться','target'=> "_blank", 'data-toggle'=>'tooltip']);

            $que = Html::a('<button class="btn btn-primary btn-xs" data-introindex="8-5"><span class="fa fa-shopping-cart"></span></button>', ['/resume/buy-resume', 'id' => $data->id],
                ['role'=>'modal-remote','title'=> 'Купить результат', 'data-toggle'=>'tooltip']);

            $star = '';
            for ($i1 = 0; $i1 < $data->mark; $i1++){
                $star .= "</span> <i class='glyphicon glyphicon-star'></i>";
            }
            $url = Url::to(['/resume/view', 'id' => $data->id]);
            return '<button class="btn btn-info btn-xs" style="cursor:auto">'.$data->fio.'</button>' .
                "  <i class='glyphicon glyphicon-signal' data-introindex=\"8-2\"></i> <span class='success'  >".($data->balls + $data->ball_for_question)." {$star} ".
                 '<span class="pull-right">'. '<span class="fa fa-ruble"></span> '. $sales . '&nbsp; '. $share . '&nbsp; '. $que  .'  <br/><div style="margin-top:10px;" data-introindex="8-7">' .
                 $data->questionary->vacancy->name .
                 '</span></div>';
        }
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'questionary.vacancy_id',
        'header' => 'Вакансия',
        'content' => function ($data) {
            return $data->questionary->vacancy->name;
        },
    ], */
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'id',
    ], */   
];   