<?php

use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Questions;
use kartik\sortinput\SortableInput;

$active = [];

foreach (json_decode($model->values) as $value) {
    $question = Questions::findOne($value->question);
    if($question != null){
        if($question->type != 6){
            $name = $question->getSelectesItem($value->value, $model->ball_for_question);
            $active += [
                $question->id => ['content' => $name],
            ];
        }
    }
}

$share = Html::a('<button class="btn btn-warning btn-xs" data-introindex="8-4"><span class="glyphicon glyphicon-share"></span></button>', ['/'.$model->code], ['data-pjax' => '0','title'=>'Поделиться','target'=> "_blank", 'data-toggle'=>'tooltip']);

$url = Url::to(['/resume/view', 'id' => $model->id]);
$update = Html::a('<button class="btn btn-info btn-xs" data-introindex="8-6"><span class="glyphicon glyphicon-eye-open"></span></button>', $url, ['data-pjax'=>'0','title'=>'Изменить', 'target' => '_blank', 'data-toggle'=>'tooltip']);

?>
        <div style="margin: 10px; max-height: 600px; overflow: auto;">                              
            <?= SortableInput::widget([
                'name'=>'active',
                'id'=>'actives',
                'items' => $active,
                'hideInput' => true,
                'sortableOptions' => [
                    'connected'=>true,
                    'itemOptions'=>['class'=>'', 'style' => 'background-color: #dff0d8;'],
                ],
                'options' => ['class'=>'form-control', 'readonly'=>true]
            ])?>
        </div>