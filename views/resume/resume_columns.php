<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'kartik\grid\ExpandRowColumn',
        'width'=>'50px',
        'contentOptions' =>['data-introindex' => "8-12"],
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($model, $key, $index, $column) {
            return \Yii::$app->controller->renderPartial('resume_questions', ['model'=>$model]);
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => "avatar",
        'width' =>'60px',
        'content' => function ($data) {
            if($data->avatar == null) $path = 'https://' . $_SERVER['SERVER_NAME'] . '/images/nouser.png';
            else $path = 'https://' . $_SERVER['SERVER_NAME'] . '/' . $data->avatar;

            return '<img style="width: 60px;border-radius: 1em;border: solid 1px #cecece;" src="' . $path . '" >';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telephone',
        'header' => 'Баллы',
        'content' => function($data){
            return $data->balls + $data->ball_for_question;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'group_id',
        'content' => function($data){
            return $data->group->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status_id',
        'content' => function($data){
            return $data->status->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'code',
        'content' => function($data){
            return Html::a( 'https://' . $_SERVER['SERVER_NAME'] . '/' . $data->code , [ '/'.$data->code ], ['data-pjax'=>'0', 'title'=> 'Поделить тест', 'target'=>'_blank', ]);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
        //'width' => '350px',
        'header' => '<div style="min-width:50px;">Действие</div>',
        //'header' => 'ФИО / Балы',
        'content' => function($data){
            /*if($data->connect_telegram == 1) $icon = ' <span class="fa fa-envelope-o"></span>';
            else $icon = '';*/
            if($data->is_new) $is_new = ' <span style="color:red;" data-introindex="8-3">new</span> ';
            else $is_new = '<span data-introindex="8-3">&nbsp;</span> ';

            $result = [];
            foreach (json_decode($data->tags) as $value) {
                $result [] = $value->id;
            }
            /*$url = Url::to(['/resume/add-resume', 'id' => $data->id]);
            $add = Html::a('<button class="btn btn-info btn-xs"><span class="fa fa-magic"></span></button>', $url, ['role'=>'modal-remote','title'=>'Предложить тест', 'data-toggle'=>'tooltip']);*/

            $url = Url::to(['/resume/print', 'id' => $data->id]);
            $print = Html::a('<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-print"></span></button>', $url, ['data-pjax'=>'0','title'=>'Печать', 'target' => '_blank', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/resume/view', 'id' => $data->id]);
            $update = Html::a('<button class="btn btn-info btn-xs" data-introindex="8-6"><span class="glyphicon glyphicon-eye-open"></span></button>', $url, ['data-pjax'=>'0','title'=>'Изменить', 'target' => '_blank', 'data-toggle'=>'tooltip']);

            //$url = Url::to(['/resume/view', 'id' => $data->id]);
            $share = Html::a('<button class="btn btn-warning btn-xs" data-introindex="8-4"><span class="glyphicon glyphicon-share"></span></button>', ['/'.$data->code], ['data-pjax' => '0','title'=>'Поделиться','target'=> "_blank", 'data-toggle'=>'tooltip']);

            $que = Html::a('<button class="btn btn-primary btn-xs" data-introindex="8-5"><span class="glyphicon glyphicon-file"></span></button>', ['/questionary/questions', 'id' => $data->questionary_id],
                ['data-pjax'=>'0','title'=> 'Открыть Анкету '.$data->questionary->name, 'target' => '_blank' , 'data-toggle'=>'tooltip']);
            $url = Url::to(['/resume/delete', 'id' => $data->id]);
            $delete = Html::a('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>', $url,
                [
                    'role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Подтвердите действие',
                    'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                ]);
            $star = '';
            for ($i1 = 0; $i1 < $data->mark; $i1++){
                $star .= "</span> <i class='glyphicon glyphicon-star'></i>";
            }
            $sales = '';
            if($data->sales !== null) $sales = '<span class="fa fa-ruble"></span> '. $data->sales . '&nbsp; ';
            $url = Url::to(['/resume/view', 'id' => $data->id]);
            return $sales. '&nbsp;' . $print . '&nbsp;'. $update . '&nbsp; '. $share . '&nbsp; '. $que . '&nbsp; ' . $delete;
        }
    ],
];   