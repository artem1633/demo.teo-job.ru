<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Resume */
/* @var $form yii\widgets\ActiveForm */
$result = [];
foreach (json_decode($model->tags) as $value) {
    $result [] = $value->id;
}
$model->tags = $result;
?>

<div class="resume-form" style="padding:20px;">

    <?php $form = ActiveForm::begin(); ?>
    
    <div style="display: none;">
        <?= $form->field($model, 'telegram_chat_id')->textInput(['maxlength' => true]) ?>   
        <?= $form->field($model, 'connect_telegram')->dropDownList($model->getConnectList(),[]) ?>
        <?= $form->field($model, 'code')->textInput(['disabled' => true]) ?>   
        <?= $form->field($model, 'new_sms')->dropDownList($model->getNewSmsList(),[]) ?>
        <?= $form->field($model, 'correspondence')->dropDownList($model->getCorrespondenceList(),[]) ?>  
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'group_id')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getGroupList(),
                'size' => 'sm',
                'options' => ['placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                ],
            ])?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status_id')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getStatusList(),
                'size' => 'sm',
                'options' => ['placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                ],
            ])?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'vacancy_id')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getVacancyList(),
                'size' => 'sm',
                'options' => ['placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                ],
            ])?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'fit')->dropDownList($model->getFitList(),[]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'mark')->dropDownList($model->getMarkList(),[]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'ball_for_question')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'doptest')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getResumeList(),
                'size' => 'sm',
                'options' => ['placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                ],
            ])?>
        </div>
    </div>
   

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'tags')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getTagsList(),
                'options' => ['placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                    'multiple' => true,
                ],
            ])?>            
        </div>
        <div class="col-md-3">
            <div style="margin-top: 25px;">
                <?= $form->field($model, 'show_in_shop')->checkBox() ?>                
            </div>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
