<?php
use yii\helpers\Html,
    yii\helpers\Url,
    app\components\helpers\FunctionHelper,
    kartik\grid\GridView,
    yii\bootstrap\Modal,
    app\models\Users,
    johnitvn\ajaxcrud\CrudAsset,
    app\models\PaymentOrders;

CrudAsset::register($this);

?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Мои выплаты</h3>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $refPaymentsProvider,
            //'filterModel' => $refPaymentsSearchModel,
            'pjax' => true,
            'responsiveWrap' => false,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'toolbar' => [
                ['content' => Yii::$app->user->identity->type == 1
                    ? Html::a('Заказать выплату', ['add-order'], [
//                        'role' => 'modal-remote',
                        'title' => 'Заказать выплату',
                        'class' => 'btn btn-warning btn-sm'
                    ]) : null],
            ],
            'columns' => [

                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'company_id',
                    'content' => function ($model) {
                        $company = Users::findOne($model->company_id);
                        return $company->fio;
                    },
                    'visible' => Yii::$app->user->identity->type == 0,
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'date_order',
                    'content' => function ($model) {
                        return date('H:i d.m.Y', strtotime($model->date_order) );
                    }
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'date_payment',
                    'content' => function ($model) {
                        return date('H:i d.m.Y', strtotime($model->date_payment) );
                    }
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'amount',
                    'content' => function ($model) {
                        return number_format($model->amount, 2, ',', ' ') . ' <i class="fa fa-rub"></i>';
                    },
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'type',
                    'content' => function ($model) {
                        switch ($model->type) {
                            case PaymentOrders::TYPE_CARD;
                                return 'На карту';
                            case PaymentOrders::TYPE_YANDEX;
                                return 'Яндекс Деньги';
                            case PaymentOrders::TYPE_BALLANCE;
                                return 'На балланс';
                            default:
                                return null;
                        }
                    }
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'requisites',
                    'content' => function ($model) {
                        return $model->requisites;
                    }
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'status',
                    'content' => function ($model) {
                        switch ($model->status) {
                            case PaymentOrders::STATUS_SUCCESS;
                                return 'Выплачена';
                            default:
                                return 'Заказана';
                        }
                    }
                ],

                [
                    'class' => '\kartik\grid\ActionColumn',
                    'headerOptions' => ['width' => '160'],
                    'template' => '{status}  {delete}',
                    'buttons' => [
                        'status' => function ($url, $model) {
                            $url = Url::to(['/affiliate-program/exchange', 'id' => $model->id]);
                            return (Yii::$app->user->identity->type == 0 && $model->status != PaymentOrders::STATUS_SUCCESS)
                                ? Html::a('<span class="fa fa-check"></span>', $url, [
                                    'class' => 'btn btn-warning btn-sm',
                                    'role' => 'modal-remote',
                                    'title' => 'Перевести',
                                    'data-confirm' => true,
//                                    'data-method' => false,// for overide yii data api
//                                    'data-request-method' => 'post',
                                    'data-toggle' => 'tooltip',
                                    'data-confirm-title' => 'Подтверждение перевода',
                                    'data-confirm-message' => 'Вы увеерены что хотите осуществить этот перевод?',
                                ])
                                : null;
                        },
                        'delete' => function ($url, $model) {
                            $url = Url::to(['delete-order', 'id' => $model->id]);
                            return $model->status != PaymentOrders::STATUS_SUCCESS
                                ? Html::a('<span class="fa fa-trash-o"></span>', $url, [
                                    'class' => 'btn btn-danger btn-sm',
                                    'role' => 'modal-remote',
                                    'title' => 'Перевести',
                                    'data-confirm' => true,
//                                    'data-method' => false,// for overide yii data api
//                                    'data-request-method' => 'post',
                                    'data-toggle' => 'tooltip',
                                    'data-confirm-title' => 'Подтвердите действие',
                                    'data-confirm-message' => 'Вы уверены что хотите удалить эту заявку?',
                                ])
                                : null;
                        },
                    ]
                ]

            ],
            'showPageSummary' => true,
        ]); ?>
    </div>
    <div class="panel-footer"></div>
</div>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>

