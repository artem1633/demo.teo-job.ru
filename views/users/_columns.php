<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'login',
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type',
        'content' => function($data){
            return $data->getTypeDescription();
        }
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telephone',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'utm',
        'label' => 'Страница компании',
        'content' => function($data){
            return Html::a( 'https://' . $_SERVER['SERVER_NAME'] . '/' . $data->utm , [ '/'.$data->utm ], ['data-pjax'=>'0', 'title'=> 'Создать', 'target'=>'_blank', ]);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telegram_id',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'main_balance',
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'questionary_sum',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'resume_sum',
    ],*/
      [
        'class'=>'\kartik\grid\DataColumn',
       'attribute'=>'buttons',
        'width'=>'100px',
        'label' => 'Действия',

        'content' => function($data){
            $url = Url::to(['/users/view', 'id' => $data->id]);
            $update = Html::a('<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>', $url, ['data-pjax'=>'0','title'=>'Изменить', 'target' => '_blank', 'data-toggle'=>'tooltip']);
                if($model->id != 1){
            $url = Url::to(['/users/delete', 'id' => $data->id]);
            $delete = Html::a('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>', $url, 
                    [
                        'role'=>'modal-remote','title'=>'Удалить', 
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ]);
        }

            return '<center>' . $update . '&nbsp; ' . $delete . '</center>';

        }
    
        // 'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        // 'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        // 'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
        //                   'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
        //                   'data-request-method'=>'post',
        //                   'data-toggle'=>'tooltip',
        //                   'data-confirm-title'=>'Подтвердите действие',
        //                   'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'], 
    ],

];   