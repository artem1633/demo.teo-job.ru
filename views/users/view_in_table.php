<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>
<div class="users-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fio',
            'login',
            [
                'attribute' => 'type',
                'value' => function($data){
                    return $data->getTypeDescription();
                }
            ],
            'telegram_id',
        ],
    ]) ?>

</div>
