<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\additional\FaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Инструкция';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="">
    <div class="col-md-12" style="padding-top: 20px;">
        <div class="panel panel-success panel-hidden-controls" >
            <div class="panel-heading ui-draggable-handle">
                <h1 class="panel-title"> 
                    <b>Инструкция</b>
                </h1>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <?=$setting->text?>
            </div>      
            <div class="panel-footer">
            </div>
        </div>
    </div>
</div>