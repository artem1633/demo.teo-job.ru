<?php

use app\models\Lessons;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Questionary */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questionary-form" style="padding: 20px;">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true,'data-introindex'=>'3-1']) ?>
        </div>
        <div class="col-md-4" data-introindex='3-2'>
            <?= $form->field($model, 'vacancy_id')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getVacancyList(),
                'options' => ['placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                ],
            ])?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'type')->dropDownList($model->getType(),['data-introindex'=>'3-3']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" data-introindex='3-4'>
            <?= $form->field($model, 'description')->widget(CKEditor::className(),[
                'editorOptions' => [
                    'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false, //по умолчанию false
                    'height' => '250px',
                ],
            ]);
            ?>
        </div>
    </div>
        <div class="row">
            <?php if($model->type == 2  && !$model->isNewRecord) { ?>
                <div class="col-md-4">
                    <?= $form->field($model, 'time_test')->textInput(['type' => 'number']) ?>
                </div>
            <?php } ?>
            <div class="col-md-4" style="padding-top: 25px;">
                <?= $form->field($model, 'publish_answer')->checkBox(['id' => 'publish_answer']) ?>
            </div>
            <div class="col-md-4" id="sum">
                <?= $form->field($model, 'result_resume_sum')->textInput(['type' => 'number']) ?>
            </div>
            <?php if(Yii::$app->user->identity->type == 0 && !$model->isNewRecord) { ?>
            <div class="col-md-4">
                <?= $form->field($model, 'is_template')->checkBox() ?>
            </div>
            <?php } ?>
        </div>
    <?php if(Yii::$app->user->identity->type == 0) { ?>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'user_id')->dropDownList($model->getUsersList(),[]) ?>
            </div>
            
        </div>
    <?php } ?>
    <?= $form->field($model, 'step')->hiddenInput()->label(false) ?>
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php

$lessons3 = Lessons::getLessonsGroup(3);
?>
<script type="text/javascript">
    function startIntro3(){
        var intro = introJs();
        intro.setOptions({
            nextLabel:"Дальше",
            prevLabel:"Назад",
            skipLabel:"Пропустить",
            doneLabel:"Понятно",
            exitOnOverlayClick:true,
            disableInteraction:true,
            overlayOpacity:1,
            steps: [
                <?php
                foreach ($lessons3 as $lesson)
                    echo '{element: document.querySelectorAll(\'[data-introindex="3-'.$lesson['step'].'"]\')[0],'.
                        'intro: "'.$lesson['hint'].'"},';?>

            ]
        });
        intro.start();
    }
</script>

<?php 
$this->registerJs(<<<JS
    var checked = $('#publish_answer').is(':checked');
    if(checked == 0) $('#sum').hide();
    else $('#sum').show();

    $('#publish_answer').on('change', function() 
    {
        var checked = $('#publish_answer').is(':checked');
        if(checked == 0) {
            $('#sum').hide();
        }
        else {
            $('#sum').show();
        }
    });
JS
);
?>
