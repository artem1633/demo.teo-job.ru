<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Questionary */
?>
<div class="questionary-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
