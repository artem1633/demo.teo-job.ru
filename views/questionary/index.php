<?php

use app\models\Lessons;
use app\models\LessonsUsers;
use app\models\Settings;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QuestionarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тесты'; 
$this->params['breadcrumbs'][] = $this->title;

if($is_template == 1) $heading = 'Список шаблоны тестов';
else $heading = 'Список тестов';
CrudAsset::register($this);

?>

        <div class="questionary-index">
            <div id="ajaxCrudDatatable">
                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'responsiveWrap' => false, 
                    'pjax'=>true,
                    'columns' => require(__DIR__.'/_columns.php'),
                    'toolbar'=> [
                        ['content'=>
                            '<div style="margin-top:10px;">' .
                            Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['create', 'is_template' => $is_template],
                            ['role'=>'modal-remote','title'=> 'Добавить', 'class'=>'btn btn-info', 'data-introindex'=>"2-1"]).
                            '<ul class="panel-controls">
                                <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                            </ul>  '.
                            '</div>'
                        ],
                    ],          
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,          
                    'panel' => [
                        'type' => 'warning', 
                        'heading' => '<i class="glyphicon glyphicon-list"></i> '.$heading .
                            '<button  type="button" class="btn btn-xs btn-warning" onclick="startLearn();">Начать обучение</button></li>',
                        'before'=>'',
                        'after'=>BulkButtonWidget::widget([
                                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>Удалить все',
                                        ["bulk-delete"] ,
                                        [
                                            "class"=>"btn btn-danger btn-xs",
                                            'role'=>'modal-remote-bulk',
                                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                            'data-request-method'=>'post',
                                            'data-confirm-title'=>'Подтвердите действие',
                                            'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                                        ]),
                                ]).                        
                                '<div class="clearfix"></div>',
                    ]
                ])?>
            </div>
        </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>


<?php
$start = 0;
$lessons_pass = LessonsUsers::getLessonsStatus(2);
$lessons_pass3 = LessonsUsers::getLessonsStatus(3);
if (!$lessons_pass) {
    $start = 1;
    LessonsUsers::setPassed(2);
}
$lessons2 = Lessons::getLessonsGroup(2);
?>
<script type="text/javascript">
    $("#ajaxCrudModal").on('shown.bs.modal', function() {
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl . '/intro/get-passed' ?>',
            type: 'post',
            data: {
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>',
                group: 3,
            },
            success: function (data) {
                if (data.passed==0) {
                    startIntro3();
                    $.ajax({
                        url: '<?php echo Yii::$app->request->baseUrl . '/intro/set-passed' ?>',
                        type: 'post',
                        data: {
                       _csrf: '<?=Yii::$app->request->getCsrfToken()?>',
                        group: 3,
                        },
                        success: function (data) {
                            console.log(data.checked);
                        }
                    });
                }
            }
        });
    });

    $(document).ready(function() {
        var start = <?=$start?>;
        if (start == 1) {
            startIntro2();
        }
    });

    function startIntro2(){
        var intro = introJs();
        intro.setOptions({
            nextLabel:"Дальше",
            prevLabel:"Назад",
            skipLabel:"Пропустить",
            doneLabel:"Понятно",
            exitOnOverlayClick:true,
            disableInteraction:true,
            steps: [
                <?php
                foreach ($lessons2 as $lesson)
                    echo '{element: document.querySelectorAll(\'[data-introindex="2-'.$lesson['step'].'"]\')[0],'.
                        'intro: "'.$lesson['hint'].'"},';?>

            ]
        });
        intro.start();
    }

    function startLearn(){
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl . '/intro/reset-passed' ?>',
            type: 'post',
            data: {
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>',
                group: 3,
            },
            success: function (data) {
                console.log(data.checked);
            }
        });
        startIntro2();
    }

</script>
