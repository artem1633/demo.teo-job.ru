<?php
use yii\bootstrap\ActiveForm;
?>
<html>
<body>
<div class="anketa-body">
    <div class="row">
        <div class="col-md-12">
            <div class="anketa-title" style="text-align: center;" ><h1><strong><?=$questionary->name?></strong></h1></div>
        </div>
        <div class="col-md-12">
            <?=$questionary->description?>
        </div>
        <div class="block">
            <?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>
            <?php
            foreach ($questions as $question)
            {
                if($question->require) $require = '<span style="color:red">*</span>';
                else $require = '';
                ?>
                <div class="row anketa_form">
                    <?php
                        if ($question->type != 6) {
                            echo '<label class="col-md-12 control-label">'.$question->name . $require.'</label>';
                            echo '<label class="col-md-12 control-label">'.$question->description.'</label>';
                        }
                    ?>
                    <?php
                    if($question->type == 0)
                    {
                        ?>
                    <div class="col-md-12">
                            <textarea class="form-control" rows="1">&nbsp;</textarea>
                        </div>
                        <?php
                    }
                    if($question->type == 1)
                    {
                        ?>
                        <div class="col-md-12">
                            <textarea  class="form-control" rows="1">&nbsp;</textarea>
                        </div>
                        <?php
                    }
                    if($question->type == 2)
                    {
                        ?>
                        <div class="col-md-12">
                            <input type="number" class="form-control">
                        </div>
                        <?php
                    }
                    if($question->type == 3)
                    {
                        ?>
                        <?php
                        foreach (json_decode($question->individual) as $value) {
                            ?>
                                <div class="col-md-12">
                                    <input type="radio" style="width:20px;height:20px;" >
                                    <label>  <?=$value->value?></label>
                                </div>
                        <?php } ?>
                        <?php
                    }
                    if($question->type == 4)
                    {
                        ?>
                        <?php
                        foreach (json_decode($question->multiple) as $value) {
                            ?>
                                <div class="col-md-12">
                                    <span  style="display: block; width:20px;height:20px; border: 1px solid black;" >&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    <label style="margin-top: 5px;"> <?=$value->question?></label>
                                </div>
                        <?php } ?>
                        <?php
                    }
                    if($question->type == 5)
                    {
                        ?>
                        <div class="col-md-12">
                            <input type="date" class="form-control">
                        </div>
                <?php } ?>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <hr class="line">
                    </div>
                </div>
                <?php
            }
            ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
</body>
</html>