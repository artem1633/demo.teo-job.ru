<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Vacancy;
use app\models\Users;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use app\models\Resume;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'kartik\grid\ExpandRowColumn',
        'width'=>'50px',
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($model, $key, $index, $column) {
            return \Yii::$app->controller->renderPartial('_question', ['model'=>$model]);
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        //'width' => '1200px',
        'header' => '<div style="min-width:600px;">Название / Короткая ссылка</div>',
        //'header' => 'Название / Короткая ссылка',
        'content' => function($data){
            $url = Url::to(['/questionary/print', 'id' => $data->id]);
            $print = Html::a('<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-print"></span></button>', $url, ['data-pjax'=>'0','title'=>'Печать', 'target' => '_blank', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/questionary/questions', 'id' => $data->id]);
            $update = Html::a('<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>', $url, ['data-pjax'=>'0','title'=>'Изменить', 'target' => '_blank', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/questionary/copy', 'id' => $data->id]);
            $copy = Html::a('<button class="btn btn-info btn-xs"><i class="fa fa-files-o"></i></button>', $url, ['role'=>'modal-remote','title'=>'Копировать', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/questions/sorting', 'questionary_id' => $data->id]);
            $sorting = Html::a('<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-sort"></span></button>', $url, ['role'=>'modal-remote','title'=>'Поменять место вопросов', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/alert/create', 'questionary_id' => $data->id]);
            $envelope = Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-envelope"></span></button>', $url, ['role'=>'modal-remote','title'=>'Cделать рассылку', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/questionary/delete', 'id' => $data->id]);
            $delete = Html::a('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>', $url,
                [
                    'role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Подтвердите действие',
                    'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                ]);

            $result = '';
            $count = Resume::find()->where(['questionary_id' => $data->id, 'connect_telegram' => 1 ])->count();
            if($data->filling_count != null) $result =
                "<span class='success'>{$data->count} <i class='glyphicon glyphicon-eye-open '></i></span> / "
                . "<span style='color:#95b75d'>{$data->filling_count} <i class='glyphicon glyphicon-file'></i></span> / "
                . "<span style='color:orange'>{$count} <i class='glyphicon glyphicon-envelope'></i></span> / ";
            else $result =
                "<span class='success'>{$data->count} <i class='glyphicon glyphicon-eye-open '></i></span> / "
                . "<span style='color:orange'>{$count} <i class='glyphicon glyphicon-envelope'></i></span> / ";

            $url = Url::to(['/resume/index', 'questionaryId' => $data->id ]);
            $data->type != 1 ? $typeT = "<i class='glyphicon  glyphicon-stats '></i>" : $typeT = "<i class='glyphicon glyphicon-list-alt '></i>";
            return Html::a('<button class="btn btn-info btn-xs">'.$typeT.' '.$data->name.'</button>', $url, ['data-pjax'=>'0','title'=>'Изменить', 'target' => '_blank', 'data-toggle'=>'tooltip']) .'<span class="pull-right"><span>' . $result . '</span> &nbsp; ' .$print. '&nbsp; '.$update . '&nbsp; ' . $copy . '&nbsp; ' . $sorting . '&nbsp; ' . $envelope . '&nbsp; ' . $delete . '</span>' .
                '<p></p>' . Html::a( 'https://' . $_SERVER['SERVER_NAME'] . '/' . $data->link , [ '/'.$data->link ], ['data-pjax'=>'0', 'title'=> 'Создать', 'target'=>'_blank', ]) . '<span class="label label-success pull-right">' . $data->vacancy->name . '</span>';
        }
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'questionaryCount',
        'label' => 'Ответов',
        'content' => function($data){
            $count = Resume::find()->where(['questionary_id' => $data->id, 'connect_telegram' => 1 ])->count();
            if($data->filling_count != null) return $data->count . ' / ' . Html::a( '<button class="btn btn-info btn-xs"> '.$data->filling_count.'</button>' , [ '/resume/index', 'questionaryId' => $data->id ], ['data-pjax'=>'0', 'title'=> 'Список резюме',  ]) . ' / ' . $count;
            else return $data->count . ' / ' . ' / ' . $count;
        }
    ], */
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'vacancy_id',
        'header' => 'Вакансия / Ответов',
        'filter' => ArrayHelper::map(Vacancy::find()->all(),'id','name'),
        'content' => function($data){
            $result = '';
            $count = Resume::find()->where(['questionary_id' => $data->id, 'connect_telegram' => 1 ])->count();
            if($data->filling_count != null) $result = $data->count . ' / ' . Html::a( '<button class="btn btn-info btn-xs"> '.$data->filling_count.'</button>' , [ '/resume/index', 'questionaryId' => $data->id ], ['data-pjax'=>'0', 'title'=> 'Список резюме' ]) . ' / ' . $count;
            else $result = $data->count . ' / ' . ' / ' . $count;
            return $data->vacancy->name . '<br/>' . $result;
        }
    ],*/
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'link',
        'content' => function($data){
            return Html::a( 'https://' . $_SERVER['SERVER_NAME'] . '/' . $data->link , [ '/'.$data->link ], ['data-pjax'=>'0', 'title'=> 'Создать', 'target'=>'_blank', ]);
        }
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'visible' => Yii::$app->user->identity->type == 0,
        'filter' => ArrayHelper::map(Users::find()->all(),'id','fio'),
        'content' => function($data){
            if($data->user_id != null) return $data->user->fio . ' ('.$data->user->getTypeDescription().')';
        }
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'access',
        'filter' => [ 0 => 'Нет', 1 => 'Да'],
        'content' => function($data){
            return $data->getAccessDesription();
        }
    ],
      [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'publish_company',
         'label' => 'Публиковать',
        'filter' => [ 0 => 'Нет', 1 => 'Да'],
        'content' => function($data){
            return $data->getPublishCompany();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'show_in_desktop',
        'label' => 'Показ',
        'filter' => [ 0 => 'Нет', 1 => 'Да'],
        'content' => function($data){
            return $data->getShowDesription();
        }
    ],*/
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'date_cr',
    // ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'buttons',
        'width'=>'200px',
        'label' => 'Действие',
        'content' => function($data){
            $url = Url::to(['/questionary/questions', 'id' => $data->id]);
            $update = Html::a('<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>', $url, ['data-pjax'=>'0','title'=>'Изменить', 'target' => '_blank', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/questionary/copy', 'id' => $data->id]);
            $copy = Html::a('<button class="btn btn-info btn-xs"><i class="fa fa-files-o"></i></button>', $url, ['role'=>'modal-remote','title'=>'Копировать', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/questions/sorting', 'questionary_id' => $data->id]);
            $sorting = Html::a('<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-sort"></span></button>', $url, ['role'=>'modal-remote','title'=>'Поменять место вопросов', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/alert/create', 'questionary_id' => $data->id]);
            $envelope = Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-envelope"></span></button>', $url, ['role'=>'modal-remote','title'=>'Cделать рассылку', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/questionary/delete', 'id' => $data->id]);
            $delete = Html::a('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>', $url, 
                    [
                        'role'=>'modal-remote','title'=>'Удалить', 
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ]);

            return '<center>' . $update . '&nbsp; ' . $copy . '&nbsp; ' . $sorting . '&nbsp; ' . $envelope . '&nbsp; ' . $delete . '</center>';
        }
    ],*/

];   